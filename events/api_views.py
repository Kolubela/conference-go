from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.acls import get_photo, get_weather_data
# ----------------------------------------------------------------------------------------------
    # CONFERENCES 
         #  THE CONFERENCE LIST AND ASSOCIATED DETAILS
#  PART A : CONFERENCE DETAILS BREAKDOWN 
class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name"
    ] 

# The Conference Detail Encoder
class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",   
    ]
    encoders = {
        "location": LocationDetailEncoder() 
    } 
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id): 
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        city = conference.location.city
        state = conference.location.state.abbreviation

        weather = get_weather_data(city, state)
        
        return JsonResponse(
            {
                "conference" : conference,
                "weather" : weather
            },
            encoder=ConferenceDetailEncoder,
            safe=False,
            ) 


    elif request.method == "DELETE":
        count, __ = Conference.objects.filter(id=id).delete()
        return JsonResponse({
            "deleted" : count > 0 
        })
    
    else: 
        content = json.loads(request.body)

        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location id"},
                status=400, 
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference, 
            encoder=ConferenceDetailEncoder,
            safe=False
        )

# Part B : Conference List 
class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name" , "description"]

@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences" : conferences},
            encoder = ConferenceListEncoder
        )
    else: 
        content = json.loads(request.body)  # We get a python dictionary as a result. 

        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
                )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference, 
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
# ----------------------------------------------------------------------------------------------




# ----------------------------------------------------------------------------------------------
    # LOCATION
         #  THE LOCATION LIST AND ASSOCIATED DETAILS


#  PART A : LOCATION DETAILS BREAKDOWN 

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name", 
        "city", 
        "room_count", 
        "created",
        "updated",
        "picture_url"
    ]
    def get_extra_data(self, o):
        return {"state" : o.state.abbreviation}
    
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
   if request.method== "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location, 
            encoder=LocationDetailEncoder,
            safe=False,
        )
   elif request.method == "DELETE":
        count, __ = Location.objects.filter(id=id).delete()
        return JsonResponse({
            "deleted" : count > 0
        }) 
   else:
        content = json.loads(request.body)

        try: 
           if "state" in content:
               state = State.objects.get(abbreviation=content["state"])
               content["state"] = state
       
       
        except State.DoesNotExist:
           return JsonResponse({"message": "Invalid state abbreviation"}, status=400)
        
        photo = get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)
       
        Location.objects.filter(id=id).update(**content)
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )

# Part B : Location List 
class LocationListEncoder(ModelEncoder):
    model = Location 
    properties = [
        "name",  
    ]

@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations}, 
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)  
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400, # The type of code will show when you run in it in insomnia. 
            )
        
        photo = get_photo(content["city"], content["state"].abbreviation)
        content.update(photo)

        location = Location.objects.create(**content)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )
