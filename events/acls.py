import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    headers = {"Authorization":PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search?"
    params= {
        "query": city + " " +state,  # question - Why is there a string with a space?
        "per_page": 1,
        # "page": 1 , 
        # "orientation": "landscape"
    }
    response = requests.get(url, params=params, headers=headers)  # I am not sure why this is the order as it doesn't match the syntax online. 
    content = json.loads(response.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}  # Question : Why is the [0] there?

    # Resources for the params and image from pexels
        # Resource = https://www.pexels.com/api/documentation/#photos-search
    
 # Version 1:
 
    
# def get_weather_data(city, state):
#     # headers = {"Authorization":OPEN_WEATHER_API_KEY}    # I am not sure if the api key goes in the query in the same way pexels does. 
    
#     # Part A = Attaining the coordinates
#     response = requests.get(f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state},USA&limit=1&appid={OPEN_WEATHER_API_KEY}')
#     # print("response content:", content)
#     content = json.loads(response.text) #note that .content and .text (attributes of the response) do the same thing. 
#     latitude = content[0]["lat"]
#     longitude = content[0]["lon"]

#     # Part B = Attaining the Weather Information 
#     response = requests.get(f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}")
#     content = json.loads(response.content)
#     return {
#         "temp": content["main"]["temp"],
#         "description": content["weather"][0]["description"],
#     }

 # Version 2:


def get_weather_data(city, state):
    # Part A : Attaining the coordinates
    geocoding_url = "http://api.openweathermap.org/geo/1.0/direct?"
    params = {
        "q" : f"{city},{state},USA",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    geocoding_response = requests.get(geocoding_url, params=params)
    geocoding_content = json.loads(geocoding_response.content)

    latitude = geocoding_content[0]["lat"]
    longitude = geocoding_content[0]["lon"]

    # Part B : Attaining the Weather

    weather_endpoint_url = "https://api.openweathermap.org/data/2.5/weather?"
    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial"
    }

    weather_response = requests.get(weather_endpoint_url, params=params)
    weather_content = json.loads(weather_response.content)

    return {
        "temp": weather_content["main"]["temp"], 
        "descriptions": weather_content["weather"][0]["description"]
        }

print(get_weather_data("Haines City", "FL"))